Services
  celery -A fipe worker -l info
 celery -A fipe beat -l info



  from fipe.core.models import Brand, Model, Year, ModelYear

In [2]: brand = Brand(code=1, label='Peugeot')

In [3]: brand.save()

In [4]: model = Model(brand=brand, code=1, label='3008')

In [5]: model.save()

In [6]: year = Year(code=1,label='2015, Gasolina')

In [7]: year.save()

In [8]: model_year = ModelYear(year=year, model=model)

In [9]: model_year.save()
