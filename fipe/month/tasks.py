import json

from celery.task.schedules import crontab
from celery.decorators import periodic_task
from fipe.month.models import Month
from fipe.month.service import FipeMonths


@periodic_task(run_every=(crontab(minute='*/1')), name="get_months", ignore_result=True)
def get_months():
    with open('/home/rodrigo/Dropbox/Code/Python/fipe/fipe/month/months.json', encoding='utf-8') as data_file:
        data = json.loads(data_file.read())

    for key in reversed(data):
        code = int(key['Codigo'])
        month= key['Mes'].split("/")[0].strip()
        year = int(key['Mes'].split("/")[1].strip())

        obj, created =Month.objects.get_or_create(
            code=code,
            month=month,
            year=year
        )
        #return obj, created