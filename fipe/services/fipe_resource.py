import requests

class FipeResource:
    def get_months(self):
        url = 'http://veiculos.fipe.org.br/api/veiculos/ConsultarTabelaDeReferencia'
        headers = {
            'Accept':'application/json, text/javascript, */*; q=0.01',
            'Accept-Encoding':'gzip, deflate',
            'Accept-Language':'pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',
            'Connection':'keep-alive',
            'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
            'Cookie':'_ga=GA1.3.472052299.1466616166; _gat=1',
            'Host':'veiculos.fipe.org.br',
            'Origin':'http://veiculos.fipe.org.br',
            'Referer':'http://veiculos.fipe.org.br/',
            'User-Agent':'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
            'X-Requested-With':'XMLHttpRequest'
        }

        return requests.post(url, headers=headers)


    def get_brand(ref_month, code):
        url = 'http://veiculos.fipe.org.br/api/veiculos/ConsultarMarcas'
        headers = {
            'Accept':'application/json, text/javascript, */*; q=0.01',
            'Accept-Encoding':'gzip, deflate',
            'Accept-Language':'pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',
            'Connection':'keep-alive',
            'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
            'Cookie':'_ga=GA1.3.472052299.1466616166; _gat=1',
            'Host':'veiculos.fipe.org.br',
            'Origin':'http://veiculos.fipe.org.br',
            'Referer':'http://veiculos.fipe.org.br/',
            'User-Agent':'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
            'X-Requested-With':'XMLHttpRequest'
        }

        payload = {'codigoTabelaReferencia' : ref_month, 'codigoTipoVeiculo' : code}
        return requests.post(url, headers=headers, data=payload)         

    def get_model(ref_month, code_brand):
        url = 'http://veiculos.fipe.org.br/api/veiculos/ConsultarModelos'
        headers = {
            'Accept':'application/json, text/javascript, */*; q=0.01',
            'Accept-Encoding':'gzip, deflate',
            'Accept-Language':'pt-BR,pt;q=0.8,en-US;q=0.6,en;q=0.4',
            'Connection':'keep-alive',
            'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8',
            'Cookie':'_ga=GA1.3.472052299.1466616166; _gat=1',
            'Host':'veiculos.fipe.org.br',
            'Origin':'http://veiculos.fipe.org.br',
            'Referer':'http://veiculos.fipe.org.br/',
            'User-Agent':'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36',
            'X-Requested-With':'XMLHttpRequest'
        }

        payload = {
            'codigoTipoVeiculo':'1',
            'codigoTabelaReferencia':ref_month,
            'codigoModelo':'',
            'codigoMarca':code_brand,
            'ano':'',
            'codigoTipoCombustivel':'',
            'anoModelo':'',
            'modeloCodigoExterno':''
        }
        
        return requests.post(url, headers=headers, data=payload)         

