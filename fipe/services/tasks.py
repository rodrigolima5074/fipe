import json
import time
import random
import datetime

from django.db.models import F
from celery.task.schedules import crontab
from celery.decorators import periodic_task
from fipe.services.fipe_resource import FipeResource
from fipe.core.models import Month, Brand, Model


@periodic_task(run_every=(crontab(hour=7, minute=30, day_of_week=1)), name="get_months", ignore_result=True)
def import_months():
    TRANSLATE_MONTH = {
      'janeiro' : '01', 'fevereiro' : '02', 'março' : '03', 'abril' : '04',
      'maio' : '05', 'junho' : '06', 'julho' : '07', 'agosto' : '09',
      'setembro' : '09', 'outubro' : '10', 'novembro' : '11', 'dezembro' : '12'
    }
    try: 
        request = FipeResource().get_months()
        data = request.json()
        data.reverse()

        for i in data:
            mes_ano = i['Mes'].split('/')
            obj , created = Month.objects.get_or_create( 
                code = i['Codigo'],
                month = TRANSLATE_MONTH[mes_ano[0]],
                year  = mes_ano[1]
            )
    except:
        raise('Erro ao importar meses de referencia')

@periodic_task(run_every=(crontab(hour=7, minute=30, day_of_week=1)), name="import_brand", ignore_result=True)
def import_brand():
    try:
        ref_month = Month.objects.order_by('year', 'month').last()
        request = FipeResource.get_brand(ref_month=ref_month.code, code=1)
        data = request.json()
        data.reverse()

        for i in data:
            obj, created = Brand.objects.get_or_create(
                label = i['Label'],
                code = i['Value']
            )
    except:
        raise('Erro ao importar Marcas')


@periodic_task(run_every=(crontab(minute='*/1')), name="import_model", ignore_result=True)
def import_model(): 
    try:
        ref_month = Month.objects.order_by('year', 'month').last()
        brands = Brand.objects.all()
        
        for brand in brands:
            now = datetime.datetime.now()
            last_update_in_days = (now - brand.import_date).days
            if last_update_in_days < 30:
                continue
                
            request = FipeResource.get_model(ref_month=ref_month.code, code_brand=brand.code)
            data = request.json()
                        
            for i in data['Modelos']:
                obj, created = Model.objects.get_or_create(
                    brand = brand,
                    code = i['Value'],
                    label = i['Label']     
                ) 
            brand.import_date = now
            brand.save()
            time.sleep(random.randint(360, 3600))
    except:
        raise('Erro ao importar modelo')