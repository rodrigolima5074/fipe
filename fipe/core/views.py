from django.core import serializers
from django.contrib import messages
from django.http import HttpResponseRedirect, JsonResponse, Http404
from django.shortcuts import render, get_object_or_404, get_list_or_404
from fipe.core.forms import CarForm
from fipe.core.models import Brand, Model, CarChart, UserCarChart

def home(request):
    if request.method == 'POST':
        return post(request)

    list_fipe = None    
    if request.user.is_authenticated():
        list_fipe = CarChart.objects.filter(user=request.user)

    return render(request, 'home.html', {'form' : CarForm(), 'list_fipe' : list_fipe})

def post(request):
    form = CarForm(request.POST)
    if form.is_valid():
        obj, created = CarChart.objects.get_or_create( 
          brand = form.cleaned_data['brand'], model = form.cleaned_data['model'], year  = form.cleaned_data['year'],
        )
            
        if request.user.is_authenticated():
            objCar, createdCar = UserCarChart.objects.get_or_create(
                user = request.user,
                car_chart = obj
            )
            list_fipe = CarChart.objects.filter(user=request.user)
            return render(request, 'home.html', {'form' : form, 'list_fipe' : list_fipe})

    return render(request, 'home.html', {'form' : form, 'list_fipe' : obj})
    

def get_model(request):
    codBrand = request.GET.get('codBrand', False)
    if not codBrand:
        raise Http404('I`m so sorry =(')

    brand = get_object_or_404(Brand, pk=codBrand)
    models = get_list_or_404(Model.objects.order_by('label'), brand=brand.pk)

    data = serializers.serialize('json', models)
    return JsonResponse({'data' : data})

def get_year(request):
    codModel = request.GET.get('codModel', False)
    if not codModel:
        raise Http404('I`m sorry =(')

    model = get_object_or_404(Model, code=codModel)
    years = model.years.all()
    data = serializers.serialize('json', years)
  
    return JsonResponse({'data' : data})
