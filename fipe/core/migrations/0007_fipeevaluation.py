# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-02-27 02:08
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20170225_0254'),
    ]

    operations = [
        migrations.CreateModel(
            name='FipeEvaluation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('month', models.IntegerField(max_length=2, verbose_name='Mês')),
                ('year', models.IntegerField(max_length=4, verbose_name='Ano')),
                ('fipe_evaluation', models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Valor')),
                ('car_chart', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.CarChart')),
            ],
        ),
    ]
