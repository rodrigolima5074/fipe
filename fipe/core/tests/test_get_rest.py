from django.test import TestCase
from fipe.core.forms import CarForm

class RestFipeModelTest(TestCase):
    def setUp(self):
        self.resp = self.client.get('/api/fipe/model', {'codBrand' : 44})

    def test_get(self):
        '''Status code 200'''
        self.assertEqual(self.resp.status_code, 200)

    def test_json(self):
        self.assertContains(self.resp.body, '[{')

class RestFipeYearTest(TestCase):
    def setUp(self):
        self.resp = self.client.get('/api/fipe/year', {'codModel' : 5428})

    def test_get(self):
        '''Status code 200'''
        self.assertEqual(self.resp.status_code, 200)

    def test_json(self):
        self.assertContains(self.resp.body, '[{')