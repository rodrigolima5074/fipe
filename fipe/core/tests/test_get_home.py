from django.test import TestCase
from fipe.core.forms import CarForm


class HomeTest(TestCase):
    def setUp(self):
        self.resp = self.client.get('/')

    def test_get(self):
        '''Status code 200'''
        self.assertEqual(self.resp.status_code, 200)

    def test_html(self):
        tags = (('<form', 1),
                ('<select', 3),
                ('type="submit"', 1))

        for text, count in tags:
            with self.subTest():
                self.assertContains(self.resp, text, count)

    def test_csrf(self):
        self.assertContains(self.resp, 'csrfmiddlewaretoken')

    def test_has_form(self):
        form = self.resp.context['form']
        self.assertIsInstance(form, CarForm)