from django import forms
from fipe.core.models import Brand, Model, Year

class CarForm(forms.Form):
    brand = forms.ModelChoiceField(label='Marca', empty_label='----', queryset=Brand.objects.all().order_by('label'))
    model = forms.ModelChoiceField(label='Marca', empty_label='----', queryset=Model.objects.all().order_by('label'))
    year  = forms.ModelChoiceField(label='Marca', empty_label='----', queryset=Year.objects.all().order_by('label'))