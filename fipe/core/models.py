import datetime

from django.db import models
from django.contrib.auth.models import User

class Month(models.Model):
    code = models.IntegerField('code', max_length=11, unique=True)
    month = models.IntegerField('month', max_length=11)
    year = models.IntegerField('year', max_length=4)
    created_at = models.DateTimeField('created at', auto_now_add=True)
    updated_at = models.DateTimeField('updated at', auto_now_add=True)


    def __str__(self):
       return "{}/{}".format(self.month, self.year);

    class Meta:
        verbose_name = 'Mês'
        verbose_name_plural = 'Meses'
        ordering = ('-code',)


class Brand(models.Model):
    code = models.IntegerField('Código', max_length=11, unique=True)
    label= models.CharField('Marca', max_length=11)
    import_date = models.DateTimeField('created at', default=datetime.datetime.now() + datetime.timedelta(-30)) 
    created_at = models.DateTimeField('created at', auto_now_add=True)
    updated_at = models.DateTimeField('updated at', auto_now_add=True)

    def __str__(self):
        return self.label

    class Meta:
        verbose_name = 'Marca'
        verbose_name_plural = 'Marcas'


class Year(models.Model):
    code = models.IntegerField('Código', max_length=11, unique=True)
    label = models.CharField('Year', max_length=11, default='')
    created_at = models.DateTimeField('created at', auto_now_add=True)
    updated_at = models.DateTimeField('updated at', auto_now_add=True)

    def __str__(self):
        return "{}".format(self.label);


class Model(models.Model):
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE) 
    code = models.IntegerField('Código', max_length=11, unique=True)
    label = models.CharField('Modelo', max_length=11)
    years = models.ManyToManyField(Year, through='ModelYear')
    created_at = models.DateTimeField('created at', auto_now_add=True)
    updated_at = models.DateTimeField('updated at', auto_now_add=True)

    def __str__(self):
        return self.label;

    class Meta:
        verbose_name = 'Modelo'
        verbose_name_plural = 'Modelos'
        ordering = ('-code',)


class ModelYear(models.Model):
    year = models.ForeignKey(Year, on_delete=models.CASCADE)
    model= models.ForeignKey(Model, on_delete=models.CASCADE)


class FipeEvaluation(models.Model):
    month = models.IntegerField('Mês', max_length=2)
    year = models.IntegerField('Ano', max_length=4)
    evaluation = models.DecimalField('Valor', max_digits=10, decimal_places=2)


class CarChart(models.Model):
    brand = models.ForeignKey(Brand)
    model = models.ForeignKey(Model)
    year  = models.ForeignKey(Year)
    user = models.ManyToManyField(User, through='UserCarChart', blank=True)
    evaluations = models.ManyToManyField(FipeEvaluation, through='CarChartFipeEvaluation', blank=True)

    def __str__(self):
        return "{} {} - {}".format(self.brand.label, self.model.label, self.year.label)


class CarChartFipeEvaluation(models.Model):
    car_chart = models.ForeignKey(CarChart, on_delete=models.CASCADE)
    fipe_evaluation = models.ForeignKey(FipeEvaluation, on_delete=models.CASCADE)


class UserCarChart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    car_chart = models.ForeignKey(CarChart, on_delete=models.CASCADE)