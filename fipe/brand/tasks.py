import json
from fipe import settings
from celery.task.schedules import crontab
from celery.decorators import periodic_task
from fipe.brand.models import Brand


@periodic_task(run_every=(crontab(minute='*/1')), name="get_brand", ignore_result=True)
def get_brand():

    with open(settings.BASE_DIR + '/contrib/brands.json', encoding='utf-8') as data_file:
        data = json.loads(data_file.read())

    for key in reversed(data):
        code = int(key['Value'])
        brand= key['Label'].strip()

        obj, created =Brand.objects.get_or_create(
            code=code,
            brand=brand,
        )
        #return obj, created
